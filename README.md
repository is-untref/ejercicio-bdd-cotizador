# Cotizador de Autos


Construir una aplicación de línea de comandos que a partir de las especificaciones de un vehículo calculen su coeficiente impositivo (ci) y su valor de mercado (vm).

```
# la applicación debe funcionar siendo invocada de la siguiente forma
ruby app.rb <tipo>/<cilindrada>/<kilometros>/<combustible>

# y debe generar una salida por la terminal como la siguiente
ci:<coeficiente_impositivo> & vm:<valor_de_mercado>
```


Donde:

* tipo: auto | camioneta | camion
* cilindrada: 1000| 1600 | 2000
* kilometros: número natural
* combustible: nafta | diesel | gas

Y siendo combustible:

* gas = 3
* diesel = 2
* nafta = 1


Considerando:

* coeficiente_impositivo = (precio_base * cilindrada) / 1000000
* valor_mercado auto = (coeficiente_impositivo * precio_base * combustible) / (1 + 0.001 * kilometraje)
* valor_mercado camioneta = 3 * (coeficiente_impositivo * precio_base) / (kilometraje + cilindrada) * 0.003)
* valor_mercado camion = (coeficiente_impositivo * precio_base) / (kilometraje + cilindrada + combustible) * 0.002)

Donde precio base:

* auto 1000
* camioneta 1500
* camion 2000
