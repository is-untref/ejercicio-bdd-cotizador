# frozen_string_literal: true

Dir[Pathname(File.dirname(__FILE__)).join('../../models/**/*.rb')].each {|f| require f}
